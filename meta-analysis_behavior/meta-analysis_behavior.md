# Humanities meet Experimental Sciences - Mapping Evidence-Based Medical Education and its underlying behavioral basis

Mauro Maldonato  
Ricardo Pietrobon


<!-- plosone http://en.wikipedia.org/wiki/List_of_open-access_journals  http://pressforward.org/  http://pressforward.org/ http://vixra.org/-->

## Abstract

## Introduction

Meta-analyses supporting Evidence-Based Medical Education are usually poorly based on existing behavioral research and barely discuss its results from a behavioral perspective. On the other side of the spectrum, researchers in the humanities who have a clear vision of the behavioral research landscape often do not make their results available in a format that is readily usable by experimental researchers and meta-analysts

Meta-analyses compile information from sometimes thousands of potential trials. Meta-analyses constitute the main pillar behind Educational Practice Guidelines. Guides online and in-person educational design around the world. Poor to non-existing review of the behavioral literature. And yet, researchers in the humanities write entire books on it that are not perused by experimental researchers and meta-analysts. 

<!-- Lipton theory on explanation -->

* Ontologies and graphs

In face of the relative lack of communication between the experimental and humanistic communities when it comes to meta-analyses in Medical Education, the goal of this project was to conduct a systematic review of meta-analyses of medical education, summarizing each individual article through a computational ontology and then connecting the resulting nodes to a sample of the literature in related concepts from the humanities. We also provide applications of this ontology to the creation of infographics summarizing ontology sections, the design and interpretation of new medical educational trials, and subsequent data support of individual meta-analyses.



## Methods

<!-- workflow showing step sequence -->

### Systematic review of medical education meta-analyses
<!-- restrict to 6, pubmed -->
look up reporting guidelines and actual examples of systematic reviews of meta-analyses

([Cook, 2008](http://jama.jamanetwork.com/article.aspx?articleid=182536)) -  fix dot file redundant concepts
<!-- ([US Dept Education, 2010](http://www2.ed.gov/rschstat/eval/tech/evidence-based-practices/finalreport.pdf)) -->

### Ontology engineering

<!-- UPON -->
CITO ontology
Ida

### Meta-analysis mapping to ontology

data extraction from meta-analyses - observer agreement

### Connection to humanistic texts

<!-- PRISMA -->
<!-- CC license, figshare, git -->
when articles were not specifically designed for the ontology, then sections 



### Ontology applications

#### Infographics 
summarizing ontology sections

#### Design and interpretation of new medical educational trials

#### Data support for individual meta-analyses


#### Reproducible research protocol


## Results


## Discussion