#-*- coding: utf-8 -*-
import csv
import os
import datetime
import logging
import hashlib
import elementtree.ElementTree as ET

XML_FILE = 'ingles.xml'

if __name__=='__main__':
    tree = ET.parse(XML_FILE)
    with open('ingles_.csv', 'wb') as csvfile:
        for quest in tree.findall("questao"):
            id_questao = (quest.get("id"))
            question = {}
            question['id'] = id_questao
            question['enunciado'] = quest.findtext("enunciado").strip()

            alternativaCorreta = \
             quest.findall("alternativas")[0]. \
                get("correta").strip()
            if len(alternativaCorreta) > 1:
                alternativaCorreta = \
                 alternativaCorreta.replace('c', 'v'). \
                    replace('e', 'f')
            print alternativaCorreta
            question['correct_choice'] = alternativaCorreta

            row = [question['id'].encode('utf-8'), question['enunciado'].encode('utf-8').replace("\n", "\\n"), question['correct_choice'].encode('utf-8')]
            for alt in quest.findall("alternativas/alternativa"):
                question[alt.get("letra")] = alt.text.strip()
                row.append(alt.text.strip().encode('utf-8').replace("\n", "\\n"))

            spamwriter = csv.writer(csvfile, delimiter=' ',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(row)
