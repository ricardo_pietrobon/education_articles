# Trends in earnings and unemployment rates across fields of study: An evaluation across x countries using reproducible research with dynamic analyses

http://goo.gl/5KAEkP OECD Employment and Labour Market Statistics
http://goo.gl/F4vW9q OECD Education Statistics

The objective of this study was to investigate how earnings and unemployment rates have varied from year ?? to ?? across different ?? countries.

<!-- implications for PS: when coming into a given country we should target fields of study with high earnings and low unemployment -->





[You can copy, download or print content for your own use, and you can also include excerpts from OECD publications, databases and multimedia products in your own documents, presentations, blogs, websites and teaching materials, provided that suitable acknowledgment of OECD as source and copyright owner is given. You should cite the Title of the material, © OECD, publication year (if available) and page number or URL (uniform resource locator) as applicable.](http://www.oecd.org/general/termsandconditions.htm#what)