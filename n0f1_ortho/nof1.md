# An N-of-1 randomized controlled trial comparing standard training vs. clinical expert fingerprints in educating orthopedic residents regarding diagnosis, biomechanics and therapeutic planning for {disease} following a reproducible research protocol

Ana Paulo Ferreira
Joao Ricardo Vissoci
Ricardo Pietrobon

IOT Joinville, Brazil
SporeData, US

## Abstract
<!-- write at the end -->

## Introduction


<!-- Disease prevalence, complex interrelationship among diagnosis, biomechanics/prognosis and therapeutic planning --> Despite its large prevalence and potential challenges in relation to its learning curve, to our knowledge no previous randomized controlled trials have focused on interventions that could improve the rate and quality of learning for healthcare professionals in training.

* lit review on previous education trials involving ortho residents
* How clinicians think
    * cognitive schema theory
    * situated cognition as an extension of schema theory. This extension through situated cognition could explain, for example, why clinicians will often reason based not only on their knowledge of basic sciences such as pathophysiology but instead use situational experiences such as previous clinical cases (@aydede2009cambridge).

The objective of this N-of-1 randomized controlled trial is therefore to compare standard training vs. clinical expert fingerprints in educating orthopedic residents regarding diagnosis, biomechanics and therapeutic planning for {disease}, the study design, conductance and reporting following a reproducible research approach. We hypothesized that the intervention with clinical expert fingerprints would result in better <!-- add outcomes -->



## Methods

Study reported following recommendations by the CONSORT extension for N-of-1 trials (CENT) guidelines (gabler2011n) <!-- if it is still not published by the time we submit our paper, then cite as communication with author https://docs.google.com/file/d/0B4Ke-17mTW1_MW1vRnFYcEdBbzMtM09IaUdCeEZvVmkwVTNJ/edit -->

# Ethics
Approval by IRB, informed consent

### Trial Design
* N-of-1
* 18 participants
* each period 2 weeks
* washout of 1 week
* run-in <!-- need to check -->
* No changes to the initially methods design were implemented while the trial was in course.

### Participants
* 18 residents <!-- Ana, please add characteristics such as gender, residency year, age -->
* demographic info is provided as a separate dataset in our reproducible research protocol to preserve confidentiality
* location <!-- Ana, please describe IOT in terms of number of patients, beds, available specialties, how patients from other areas might be referred there  -->


### Interventions

* clinical expert fingerprint
    * multiple sources
        * CTA interview think aloud with expert
        * CTA includes not only concepts but also situations
            * stories about previous patients - the more the better
            * gut feeling or tacit knowledge
            * tips on training
            * how people who influenced the expert do it
            * ...
        * validation with external sources of literature <!-- AO site, specific lit review articles, ... -->
    * situated schema representation
        * social net represented as group of triples - RDF
        * gephi for visualization ![](images/mock_gephi.jpg)
    * exercises through edx
        * graph translated into exercises
        * exercises presented as collection of clinical cases, clinical images, 3D pictures created in maya based on images collected from the clinicians' experience, online collections, fracture atlases, ...
        * 20 exercises
* standard training
    * rounds
    * grand rounds
    * cases
    * surgical procedures
    * clinical discussions
    * <!-- Ana, here it's important to describe exactly what their routine might be -->

<!-- add CONSORT for Non-Pharmacological Treatment Interventions -->


### Outcomes

* learning
    * ability to match AAOS' clinical practice guidelines
* experience
    * satisfaction 
    * feeling of progress
    * feeling of mastery and capacity to apply knowledge in practice


<!-- add any changes to outcomes happening during the trial  -->

### Sample size

<!-- any interim analyses if they happened -->

### Randomization

Sequence generation R and blocking if applicable
Allocation concealment
Implementation envelopes
Blinding

### Statistical methods

R language (@rcitation2014) [crossdes](http://cran.r-project.org/web/packages/crossdes/index.html) package for cross design analysis
carryover
period effects
intra-subject correlation
methods to synthesize different N-of-1
how heterogeneity between participants was assessed
check PRISMA statement since it's combining multiple trials

### Reproducible research
<!-- take from previous papers -->

## Results

<!-- add this section in alignment with CENT -->

## Discussion



