# A platform for efficacy evaluation of healthcare lifelong learning: Framework description and demonstration factorial randomized controlled trial involving <!-- number --> healthcare professionals under a reproducible research framework

Sandra M. Pelloso  
Maria D. B. Carvalho  
Joao R. Vissoci  
Ricardo Pietrobon  

<!-- pandoc -s -S --biblio nursing_survey.bib --csl chicago-author-date.csl nursing_survey.md -o nursing_survey.pdf -->

<!-- 
journalists
databases
 -->

# Abstract
<!-- write at the end -->

# Introduction

We are just beginning to have the first signs of evidence that healthcare education leads to decreased mortality rates among surgical patients ([Kutney-Lee, 2013](http://www.ncbi.nlm.nih.gov/pubmed/23459738)). However, in a world of constant change where a Baccalaureate is all but the beginning of a lifelong education path, fine grained data about specific educational interventions conducted in a real-world environment are still sorely missed.

<!-- review articles in bib file -->

The objective of this study is to describe an online education platform adapted for healthcare education trials, describing its course and evaluation components.  In addition, we present the results of a demonstration randomized controlled trial comparing personalized education in relation to evidence-based decision making in the context of specialty specific topics.

# Methods

## edX
* Consortium
* Content with videos and slides
* Assessment
* Courses available in XML

## Computerized Adaptive Testing
* [Concerto](https://code.google.com/p/concerto-platform/)
* Concomitant validation with prior

## Sample and recruitment
* [Cofen](http://www.portalcofen.gov.br/)

## Educational intervention

### Personalization


### Clinical cases
* Maternal mortality - [WHO guidelines on maternal, reproductive and women's health](http://www.who.int/publications/guidelines/reproductive_health/en/index.html)
* [Koch, 2012](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0036613)
* [Karlsen, 2011](http://www.biomedcentral.com/1471-2458/11/606)
* [McAlister, 2006](http://www.ncbi.nlm.nih.gov/pubmed/17169224)
* [Hirst, 2013](http://goo.gl/v2oQDx)

## Outcome measures
* Clinical cases, IRT-calibrated

## Randomization

## Data analysis
* R (@rcitation)
* [Sublime](), [sendtext](), [bitbucket]() for conducting the analysis 
<!-- grab from  -->

## Reproducible research protocol
In order to improve the reproducibility of our work, we used combined different reproducible research protocols [(Vissoci, 2013)](http://arxiv.org/abs/1304.5688)([Hrynaszkiewicz, 2010](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2813427/))([Jackson, 2009](http://cronus.uwindsor.ca/users/d/djackson/main.nsf/9d019077a3c4f6768525698a00593654/fd2b367204a6327385257577006619d8/$FILE/CFA%20Review%20PMETHODS(2009).pdf))([Sandve, 2013](http://www.ploscompbiol.org/article/info:doi/10.1371/journal.pcbi.1003285)). <!-- reporting guidelines for SEM/CFA and not referenced in this project http://goo.gl/9DQts http://goo.gl/vY2F0 --> Briefly, all data sets, scripts, templates, software and workflows generated under this project were deposited under the public repositories [Bibucket](https://bitbucket.org/ricardo_pietrobon/timss_bifactor) and [Figshare](). <!-- please add reference once available --> 


# Results

Figure 1. Boxplot clinical case scores by intervention

Figure 2. 

# Discussion



lifelong learning needs not met

system to track interventions by Cofen (Conselho Federal de Enfermagem, Federal Nursing Council)

baseline data

scale development

[COMPENDIUM OF ANA EDUCATION POSITIONS, POSITION STATEMENTS, AND DOCUMENTS](http://nursingworld.org/MainMenuCategories/Policy-Advocacy/State/Legislative-Agenda-Reports/NursingEducation/NursingEducationCompendium.pdf)


# References

[An Increase In The Number Of Nurses With Baccalaureate Degrees Is Linked To Lower Rates Of Post-surgery Mortality Ann Kutney-Lee, Douglas M. Sloane and Linda H. Aiken Health Affairs, Vol. 32, No. 3: 579-586 (March 2013)](http://dl.getdropbox.com/u/2773672/Health%20Aff-2013-Kutney-Lee-579-86.pdf)

Baccalaureate Education in Nursing and Patient Outcomes
Mary A. Blegen, Colleen Goode, Shin Hye Park, Thomas Vaughn, Joanne Spetz JONA, Vol. 43, No. 2 (February 2013)

<!-- plot for paper [AMERICAN NURSES ASSOCIATION© STATES WHICH REQUIRE CONTINUING EDUCATION FOR RN LICENSURE 2013](http://nursingworld.org/MainMenuCategories/Policy-Advocacy/State/Legislative-Agenda-Reports/NursingEducation/CE-Licensure-Chart.pdf)
 -->