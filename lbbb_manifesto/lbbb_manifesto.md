# Learning Beyond Brain Boundaries: A Manifesto

<!-- 
data merging and enrichment is essential 
sparql book
 -->

Learning is the basis of our society. And yet, the way most institutions convey learning is currently out of sync with society. 

This manifest calls for a fundamental change in the way learning is imparted. Our manifesto relies on Dialectics, Pragmatism, Agile and Lean movements, as well as Situated Cognition as be basis for a call to arms. 

Specifically, we propose to modify how learners process information and then convert it into action to create positive change for themselves, those close to them, and world society. 



## Processing information
Learning is often disconnected from its surrounding

* Problems
    * define what constitutes a problem: social, art, professional
    * courses are offered in areas that are not needed, resulting in youth unemployment in Europe <!-- courses in response to skill gaps oecd and skills data -->
    * but society is not equal market, quite the opposite. need for real data to avoid missing the real problems, be they social, ethical
    * universities disconnected from societal discussion as used to happen in the 60s, a prime example being universities funded from the NSA abstaining from a much needed discussion on the role of their intrusion in our lives and privacy, all because some universities have become self-centered entities, lost within the rat wheel of potential funding metrics, prostitutes at the will of their funding sources. <!-- https://www.math.columbia.edu/~woit/wordpress/?p=6522 nsa researchers having conflict of interest -- find g+ post about funds from nsa and post by professor asking why mathematicians are quiet - data on nsa funding - http://www.ams.org/notices/201311/rnoti-p1432.pdf only now first calls to action, but for years this went undisclosed --> is the university role to be a servant or to play a critical role in helping define what we as a society should become. we should be answering whether it might be ethical for us to act as consumerist bastards while others starve and suffer around the world
    * when criticized by their disconnect from the market chain, university's response is often times to offer competition to for profit companies while having the benefit of being tax exempt such as in the case of Academic Clinical Research Organizations <!-- data showing overlap: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3062324/ for argument on why ACROs should exist, check duke database for CRO market, http://www.denverpost.com/opinion/ci_23778803/hospitals-cant-have-it-both-ways for argument in relation to hospitals, http://www.aspeninstitute.org/policy-work/nonprofit-philanthropy/archives/nonprofit-philanthropy-7 on their mission being deviated by market forces , http://www.academia.edu/687068/Administrative_and_Academic_Strucures_For-Profit_and_Not-For-Profit argument in favor of combining for profit and non-profit roles , call for public private partnerships http://net.educause.edu/ir/library/pdf/pub72037.pdf -->
    * the role of pharmaceutical funding
    * inefficiency of COI measures
    * Societal causes as a way to look beyond own belly, our immediate situation
* LBBB way
    * Know how to find information from different sources <!-- json inventory of data sources, import.io, kindle data journalism -->
        * http://data.worldbank.org/
        * http://www.skillsforemployment.org/KSP/en/KnowledgeProducts/Statisticalinformation/index.htm
        * http://schoolofdata.org/handbook/courses/finding-data/
        * http://nces.ed.gov/surveys/
        * http://databases.unesco.org/
        * http://datacatalogs.org/
        * http://www.who.int/research/en/
        * http://oad.simmons.edu/oadwiki/Data_repositories
        * http://datadryad.org/
        * http://datahub.io/
        * http://datajournalismhandbook.org/1.0/en/getting_data_0.html
        * http://open-data.europa.eu/en/linked-data
        * http://www.data.gov/
        * http://dados.gov.br/
        * http://datahub.io/
        * http://www.imf.org/external/data.htm
        * http://web.worldbank.org/WBSITE/EXTERNAL/WBI/WBIPROGRAMS/KFDLP/EXTUNIKAM/0,,contentMDK:20589420~menuPK:1454007~pagePK:64168445~piPK:64168309~theSitePK:1414721,00.html
        * http://www.ilo.org/ilostat/faces/home/statisticaldata?_adf.ctrl-state=ddzmpqkra_114&clean=true&_afrLoop=1706428065122616
        * http://www.skillsforemployment.org/KSP/en/KnowledgeProducts/Statisticalinformation/index.htm
        * University specific 
            * http://library.fuqua.duke.edu/masterdb.htm
            * http://library.duke.edu/databases/
        * The Future of Personal Information Management
        * Linked Data book by bizer - dropbox
        * kindle resource oriented architecture
    * Omniscient and omnipresent

<!-- try processing, tableau, protovis, gephi, refine, jquery, google table fusion and import.io 
http://schoolofdata.org/online-resources/
http://schoolofdata.org/courses/#GentleIntroExtractingData

-->

## Acting to create change

* Problems
    * sterile discussions that results in no action. Humanities crises paradox: more needed that ever but stop short at the discussion and with no practical consequences
    * distant Africa with enduring problems while those of us in wealthy nations do nothing
    * dialectical, Agile and Lean -- need for change in practice 
    * Sense of justice and rawl
* LBBB way
    * iterative experiments focused on the immediate problems at hand
    * what defines Agile
        * focus on needs
        * small, iterative cycles
        * functional and incomplete
        * minimum viable product
    * experiments necessarily mean the evaluation of outcomes
    * learn alternative and sustainable paths - if you want to be an artist how can you create a business model that will make that life possible, digna and enjoyable
    * evaluating outcomes does not necessarily mean quantitative, but by both qualitative and quantitative means
    * although initially promulgated for Lean Startups <!-- ref --> it can and should be used for social change <!-- http://goo.gl/9aCdPi -->
    * Über analytical 
    * Über creator of still and living matter

<!-- 
matrix
graphics in svg 
initial translations: italian, english, portuguese
-->